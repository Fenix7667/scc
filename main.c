#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>

static size_t memory_read = 0, size = 500;
static char *input = "";

void get_input(void)
{
	if (memory_read != 0) {
		free(input);
	}
	memory_read = getline(&input, &size, stdin);
	if (memory_read == -1) {
		puts("ERROR!");
		exit(1);
	}
	input[strcspn(input, "\r\n")] = 0;
	return;
}

void quadratic_eq(void)
{
	double delta = 0, eq_a = 0, eq_b = -99, eq_c = -99, r1 = 0, r2 = 0;
	for (size_t index = 0; index < memory_read; ++index) {
		char token = input[index];
		if (token == ' ') {
			puts("PARSING ERROR! Spaces not allowed.");
			exit(1);
		}
		if (token >= '0' && token <= '9') {
			if (index == 0)
				eq_a = (token - '0');
			if (index > 0) {
				if (input[index -1] == '^') {
					continue;
				}
				if (eq_b == -99) {
					eq_b = (token - '0');
					if (input[index -1] == '-')
						eq_b = -eq_b;
					continue;
				}
				if (eq_c == -99) {
					eq_c = (token - '0');
					if (input[index -1] == '-')
						eq_c = -eq_c;
				}
			}
		}
	}
	
	delta = (eq_b * eq_b) - (4 * eq_a * eq_c);

	printf("\na: %f\n", eq_a);
	printf("b: %f\n", eq_b);
	printf("c: %f\n\n", eq_c);
	printf("delta: %.2f\n", delta);
	
	if (delta > 0) {
		r1 = (-eq_b + sqrt(delta)) / (2 * eq_a);
		r2 = (-eq_b - sqrt(delta)) / (2 * eq_a);
		
		printf("r1: %.2f\n", r1);
		printf("r2: %.2f\n", r2);
	}
	return;
}

int main(void)
{
	input = (char *)malloc(size);

	puts("Do you want a quadratic equation or normal calculator?");
	get_input();
	
	if (strcmp(input, "quadratic") == 0) {
		get_input();
		quadratic_eq();
	}
	free(input);
	return 0;
}
